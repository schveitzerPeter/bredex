import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AuthService } from "../service/auth.service";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    
  constructor(
    private authService: AuthService    
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if( this.authService.isLoggedin()){
      const userToken = this.authService.isLoggedin()
      const modifiedReq = req.clone({ 
        headers: req.headers.set('Authorization', `Bearer ${userToken}`),
      });
      return next.handle(modifiedReq);
    } else{
      return next.handle(req);
    }
  }


}
