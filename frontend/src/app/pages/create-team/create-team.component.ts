import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';
import { TeamService } from 'src/app/service/team.service';

@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.css']
})
export class CreateTeamComponent implements OnInit {

  constructor(
    private teamService: TeamService,
    private router: Router,
    private activeroute: ActivatedRoute
  ) { }
  hideRequiredControl = new FormControl(false);

  createTeamForm: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    foundYear: new FormControl('', [ Validators.required, Validators.minLength(4) ]),
    wins: new FormControl('', [Validators.required]),
    isPaid: new FormControl('',)
  })
  

  ngOnInit(): void {

  }



  create(){    
    if( this.createTeamForm.valid){
      this.teamService.createTeam(this.createTeamForm.value).subscribe(
        (resp:any)=>{
          if(resp.status=200){
            this.router.navigate(['home'])
          }
        }
      )              
    }    
  }
}
