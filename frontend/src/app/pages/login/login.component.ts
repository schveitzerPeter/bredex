import { Component, OnInit } from '@angular/core';
import { Form, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/model/User';
import { AuthService } from 'src/app/service/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private routerService: Router
  ) { }

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  })
  cred:boolean = false

  ngOnInit(): void {
    if(this.authService.isLoggedin()){
      this.routerService.navigate(['home'])
    }
  }

  login(){    
    if( this.loginForm.valid){
      this.authService.login(this.loginForm.value).subscribe( (resp:any)=>{        
          

          if( resp.status == 403){
            this.cred = true
          }
          else if (resp.status == 200){            
            this.authService.setToken(resp.data)
            this.routerService.navigate(['home'])
          }
        }        
      )
    }
  }

}
