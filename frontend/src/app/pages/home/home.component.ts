import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Team } from 'src/app/model/Team';
import { AuthService } from 'src/app/service/auth.service';
import { TeamService } from 'src/app/service/team.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  
  displayedColumns!: string[]

  
  dataSource!:Team[]


  show!:boolean

  constructor(
    private teamService: TeamService,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.teamService.getAllTeam().subscribe(
      (resp:any)=>{        
        this.dataSource = resp.data
      }
    )    
    if(this.isLoggedIn()){
      this.displayedColumns = ['name', 'found', 'wins', 'isPaid','actions'];
    }else{
      this.displayedColumns = ['name', 'found', 'wins', 'isPaid'];
    }
  }


  deleteTeam(id:string){
    this.teamService.deleteTeamById( id ).subscribe( (resp:any)=>{
      if(resp.status==200){
        this.updateDataSource(id)
      }
    })    
  }

  updateDataSource(id:string){
    this.dataSource = this.dataSource.filter(function( obj ) {
      return obj.teamId != id;
    });
  }

  editTeam(id:string){
    this.router.navigate(["update/"+id])
  }

  isLoggedIn(){
    return this.authService.isLoggedin()
  }

  
}
