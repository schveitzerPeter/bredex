import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TeamService } from 'src/app/service/team.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'app-update-team',
  templateUrl: './update-team.component.html',
  styleUrls: ['./update-team.component.css']
})
export class UpdateTeamComponent implements OnInit {

  constructor(
    private teamService: TeamService,
    private router: Router,
    private activeroute: ActivatedRoute
  ) { }

  hideRequiredControl = new FormControl(false);

  updateTeamForm: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    foundYear: new FormControl('', [ Validators.required, Validators.minLength(4) ]),
    wins: new FormControl('', [Validators.required]),
    isPaid: new FormControl('',)
  })
  
  teamId!:string
  ngOnInit(): void {
    this.activeroute.params.subscribe(params => {
      this.teamId = params['id']
      this.teamService.getTeamById( this.teamId ).subscribe( (resp:any)=>{
        this.updateTeamForm.get('name')?.setValue(resp.data.name)
        this.updateTeamForm.get('foundYear')?.setValue(resp.data.foundYear)
        this.updateTeamForm.get('wins')?.setValue(resp.data.wins)
        this.updateTeamForm.get('isPaid')?.setValue(resp.data.isPaid)        
      })
    });
  }
  
  update(){
    if( this.updateTeamForm.valid){
      this.teamService.updateTeam(this.updateTeamForm.value, this.teamId ).subscribe(
        (resp:any)=>{
          if(resp.status==200){
            this.router.navigate(['home'])
          }
        }
      )              
    }    
  }
}
