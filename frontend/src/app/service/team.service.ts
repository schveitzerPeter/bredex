import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Team } from '../model/Team';
import { User } from '../model/User';

@Injectable({
    providedIn: 'root'
  })
  export class TeamService {  
        
    API_PATH = "http://localhost:8080/team/";
    constructor(
        private http: HttpClient
    ){}

    getTeamById( teamId: string) {
        return this.http.get(this.API_PATH + teamId)
    }

    createTeam( team: Team){
        return this.http.post(this.API_PATH, team)
    }
   
    updateTeam(team: Team, teamId: string) {
        return this.http.put(this.API_PATH + teamId, team)
    }

    deleteTeamById(teamId: string) {
        return this.http.delete(this.API_PATH + teamId)
    }

    getAllTeam() {
        return this.http.get(this.API_PATH)
    }
}