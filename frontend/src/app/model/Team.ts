export interface Team {
  teamId: string;
  name: string;
  foundYear: number;
  wins: number;
  isPaid: boolean;
}