package bredex.forma1.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "team")
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long teamId;

    @Column(name = "name")
    private String name;

    @Column(name = "found_date")
    private Integer foundYear;

    @Column(name = "wins")
    private Integer wins;

    @Column(name = "is_paid")
    private boolean isPaid;

    @ManyToOne
    @JoinColumn(name="userid", nullable=false)
    @JsonBackReference
    private User user;

}
