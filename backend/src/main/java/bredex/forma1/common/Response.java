package bredex.forma1.common;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Response {
    private int status;
    private Object data;
    private String message;

    private Response(ResponseBuilder builder){
        this.status = builder.status;
        this.data = builder.data;
        this.message = builder.message;
    }

    public static class ResponseBuilder{

        private int status;
        private Object data;
        private String message;

        public ResponseBuilder setStatus( int status){
            this.status = status;
            return this;
        }

        public ResponseBuilder setData( Object data ){
            this.data = data;
            return this;
        }

        public ResponseBuilder setMessage( String msg){
            this.message = msg;
            return this;
        }

        public Response build(){
            return new Response(this);
        }
    }
}
