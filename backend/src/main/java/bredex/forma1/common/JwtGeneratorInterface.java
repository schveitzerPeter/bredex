package bredex.forma1.common;

import bredex.forma1.model.User;

import java.util.Map;

public interface JwtGeneratorInterface {
    Map<String, String> generateToken(User user);

    User getUserFromToken(String token);
}
