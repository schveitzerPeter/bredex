package bredex.forma1.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.apache.logging.log4j.util.Strings;

@Data
public class TeamDTO {
    private Long teamId = null;
    private String name = Strings.EMPTY;
    private Integer foundYear = 0;
    private Integer wins = 0;
    @JsonProperty("isPaid")
    private boolean isPaid = false;
}