package bredex.forma1.service;

import bredex.forma1.common.Response;
import bredex.forma1.dto.UserDTO;
import bredex.forma1.model.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    User getUserByNameAndPassword(String name, String password);

    Response login(UserDTO user);
}
