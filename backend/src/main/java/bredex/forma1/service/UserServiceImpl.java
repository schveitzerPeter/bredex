package bredex.forma1.service;

import bredex.forma1.common.JwtGeneratorImpl;
import bredex.forma1.common.JwtGeneratorInterface;
import bredex.forma1.common.Response;
import bredex.forma1.dto.UserDTO;
import bredex.forma1.model.User;
import bredex.forma1.repository.UserRepository;
import com.google.common.hash.Hashing;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

@Service
public class UserServiceImpl implements UserService{

    private UserRepository userRepository;

    private JwtGeneratorImpl jwtGenerator;

    public UserServiceImpl() {
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setJwtGenerator(JwtGeneratorImpl jwtGenerator) {
        this.jwtGenerator = jwtGenerator;
    }

    @Override
    public User getUserByNameAndPassword(String name, String password) {
        return userRepository.findByUserNameAndPassword(name, password);
    }

    public Response login(UserDTO user){
        final String hashedPassword = Hashing.sha256()
                .hashString(user.getPassword(), StandardCharsets.UTF_8)
                .toString();
        User userToAuth = getUserByNameAndPassword(user.getUsername(),hashedPassword);

        if(userToAuth == null){
            return new Response.ResponseBuilder()
                .setStatus(403)
                .setData(null)
                .setMessage("Wrong username or password!")
                .build();
        } else {
            return new Response.ResponseBuilder()
                .setStatus(200)
                .setData( jwtGenerator.generateToken(userToAuth.getUserName()) )
                .setMessage("Logged in successfully!")
                .build();
        }
    }
}
