package bredex.forma1.service;

import bredex.forma1.common.JwtGeneratorImpl;
import bredex.forma1.common.Response;
import bredex.forma1.dto.TeamDTO;
import bredex.forma1.model.Team;
import bredex.forma1.model.User;
import bredex.forma1.repository.TeamRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TeamServiceImp implements TeamService{

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private JwtGeneratorImpl jwtGenerator;
    ModelMapper modelMapper = new ModelMapper();


    public TeamServiceImp(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Override
    public Response getAllTeam() {
        List<Team> teams = teamRepository.findAll();
        return (!teams.isEmpty()) ? new Response.ResponseBuilder()
                .setStatus(200)
                .setData(
                        teams.stream().map(team -> modelMapper.map(team, TeamDTO.class)).collect(Collectors.toList())
                )
                .setMessage( teams.size() + " team have been found!")
                .build() :
                new Response.ResponseBuilder().setStatus(400).setData(null).setMessage("No teams has been found :'(!").build();
    }

    @Override
    public Response getTeamById(Long id) {
        Optional<Team> movieToFind = teamRepository.findById(id);
        return ( movieToFind.isPresent() ) ? new Response.ResponseBuilder()
                .setStatus(200)
                .setData(
                        modelMapper.map(movieToFind.get(), TeamDTO.class)
                )
                .setMessage("Team with id: "+ id + " has been found!")
                .build() :
                new Response.ResponseBuilder().setStatus(400).setData(null).setMessage("Team with id: " + id + " can not be found!").build();
    }

    @Override
    public Response deleteTeamById(Long id) {
        Optional<Team> movieToDelete = teamRepository.findById(id);

        if( movieToDelete.isPresent() ){
            teamRepository.deleteById(id);
            return new Response.ResponseBuilder()
                    .setStatus(200)
                    .setData(null)
                    .setMessage("Team with id: "+ id + " has been deleted!")
                    .build();
        }
        return new Response.ResponseBuilder()
                .setStatus(400)
                .setData(null)
                .setMessage("Team with id: " + id + " can not be found!")
                .build();
    }

    @Override
    public Response createTeam(TeamDTO teamDTO, String jwtToken) {
        User userWhoCreated = jwtGenerator.getUserFromToken(jwtToken);
        if( validateMovieDTO(teamDTO) && userWhoCreated != null) {
            Team teamToSave = modelMapper.map(teamDTO, Team.class);
            teamToSave.setUser(userWhoCreated);
            teamToSave = teamRepository.save(teamToSave);
            TeamDTO responseMovieDTO = modelMapper.map(teamToSave, TeamDTO.class);
            return new Response.ResponseBuilder()
                    .setStatus(200)
                    .setData(responseMovieDTO)
                    .setMessage("Team with title "+ responseMovieDTO.getName() +" has been created !")
                    .build();
        }
        return new Response.ResponseBuilder()
            .setStatus(400)
            .setData(null)
            .setMessage("Team can not be added due to incorrect data!")
            .build();
    }

    @Override
    public Response updateTeamById(TeamDTO teamDTO, Long id) {
        Optional<Team> movieToOptional = teamRepository.findById(id);
        if( validateMovieDTO(teamDTO) && movieToOptional.isPresent() ) {
            Team teamToModify = movieToOptional.get();

            if( !teamToModify.getName().equals(teamDTO.getName()) ){
                teamToModify.setName(teamDTO.getName());
            }

            if( !teamToModify.getFoundYear().equals(teamDTO.getFoundYear()) ){
                teamToModify.setFoundYear(teamDTO.getFoundYear());
            }

            if( !teamToModify.getWins().equals(teamDTO.getWins()) ){
                teamToModify.setWins(teamDTO.getWins());
            }

            if( teamToModify.isPaid() != teamDTO.isPaid() ){
                teamToModify.setPaid(teamDTO.isPaid());
            }

            teamRepository.save(teamToModify);

            return new Response.ResponseBuilder()
                    .setStatus(200)
                    .setData(teamToModify)
                    .setMessage("Movie modified!")
                    .build();
        }
        return new Response.ResponseBuilder()
                .setStatus(400)
                .setData(null)
                .setMessage("Movie can not be modified due to incorrect data!")
                .build();
    }

    private boolean validateMovieDTO(TeamDTO movieDTO) {
        return  !movieDTO.getName().isBlank() && !(movieDTO.getWins() < 0);
    }

    private User getUserFromJWTToken( String token){
        return jwtGenerator.getUserFromToken(token);
    }
}
