package bredex.forma1.service;

import bredex.forma1.common.Response;
import bredex.forma1.dto.TeamDTO;
import org.springframework.stereotype.Service;

@Service
public interface TeamService {
    Response getAllTeam();
    Response getTeamById( Long id);
    Response deleteTeamById( Long id);
    Response createTeam(TeamDTO teamDTO, String jwtToken);
    Response updateTeamById( TeamDTO teamDTO, Long id);
}
