package bredex.forma1.controller;

import bredex.forma1.common.Response;
import bredex.forma1.dto.TeamDTO;
import bredex.forma1.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/team")
@CrossOrigin(origins = {"http://localhost:4200", "http://localhost:4201"}, methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
public class TeamController {

    @Autowired
    private TeamService teamService;

    @GetMapping(path = "/", produces = "application/json")
    public Response getAllTeam(){return teamService.getAllTeam();}

    @PostMapping(path = "/", consumes = "application/json", produces = "application/json")
    public Response createTeam(@RequestBody TeamDTO movieDTO, @RequestHeader("Authorization") String jwtToken){
        return teamService.createTeam(movieDTO, jwtToken);
    }

    @GetMapping(path = "/{teamId}", produces = "application/json")
    public Response getTeamById(@PathVariable("teamId") Long teamId){return teamService.getTeamById(teamId);}

    @DeleteMapping(path = "/{teamId}", produces = "application/json")
    public Response deleteTeamById(@PathVariable("teamId") Long teamId){return teamService.deleteTeamById(teamId);}

    @PutMapping(path = "/{teamId}", produces = "application/json")
    public Response updateTeam(@RequestBody TeamDTO movieDTO, @PathVariable("teamId") Long teamId){return teamService.updateTeamById(movieDTO, teamId);}
}