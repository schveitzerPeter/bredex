package bredex.forma1.controller;

import bredex.forma1.common.JwtGeneratorInterface;
import bredex.forma1.common.Response;
import bredex.forma1.dto.UserDTO;
import bredex.forma1.model.User;
import bredex.forma1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/user")
public class UserController {
    private UserService userService;
    @Autowired
    public UserController(UserService userService){
        this.userService=userService;
    }

    @PostMapping("/login")
    public Response loginUser(@RequestBody UserDTO user) {
        return userService.login(user);
    }
}
